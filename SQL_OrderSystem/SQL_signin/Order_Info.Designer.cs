﻿namespace SQL_signin
{
    partial class Order_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.client = new System.Windows.Forms.TextBox();
            this.ClientName = new System.Windows.Forms.Label();
            this.choose_store = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.combobox = new System.Windows.Forms.Label();
            this.choose_product = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.perprice = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.product_amount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.little_bill = new System.Windows.Forms.Label();
            this.addintobasket = new System.Windows.Forms.Button();
            this.total = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.order_log = new System.Windows.Forms.ListBox();
            this.label87 = new System.Windows.Forms.Label();
            this.order = new System.Windows.Forms.Button();
            this.orderLogBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.orderLogBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // client
            // 
            this.client.Location = new System.Drawing.Point(162, 57);
            this.client.Name = "client";
            this.client.Size = new System.Drawing.Size(116, 22);
            this.client.TabIndex = 0;
            // 
            // ClientName
            // 
            this.ClientName.AutoSize = true;
            this.ClientName.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.ClientName.Location = new System.Drawing.Point(26, 51);
            this.ClientName.Name = "ClientName";
            this.ClientName.Size = new System.Drawing.Size(105, 24);
            this.ClientName.TabIndex = 1;
            this.ClientName.Text = "您的姓名：";
            // 
            // choose_store
            // 
            this.choose_store.FormattingEnabled = true;
            this.choose_store.Location = new System.Drawing.Point(162, 109);
            this.choose_store.Name = "choose_store";
            this.choose_store.Size = new System.Drawing.Size(147, 20);
            this.choose_store.TabIndex = 2;
            this.choose_store.SelectedIndexChanged += new System.EventHandler(this.choose_store_SelectedIndexChanged);
            this.choose_store.Click += new System.EventHandler(this.removefirst);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(26, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "請選擇店家：";
            // 
            // combobox
            // 
            this.combobox.AutoSize = true;
            this.combobox.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.combobox.Location = new System.Drawing.Point(26, 167);
            this.combobox.Name = "combobox";
            this.combobox.Size = new System.Drawing.Size(124, 24);
            this.combobox.TabIndex = 4;
            this.combobox.Text = "請選擇商品：";
            // 
            // choose_product
            // 
            this.choose_product.FormattingEnabled = true;
            this.choose_product.Location = new System.Drawing.Point(162, 171);
            this.choose_product.Name = "choose_product";
            this.choose_product.Size = new System.Drawing.Size(147, 20);
            this.choose_product.TabIndex = 5;
            this.choose_product.SelectedIndexChanged += new System.EventHandler(this.choose_product_comboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(354, 167);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 24);
            this.label2.TabIndex = 6;
            this.label2.Text = "單價：";
            // 
            // perprice
            // 
            this.perprice.AutoSize = true;
            this.perprice.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.perprice.Location = new System.Drawing.Point(427, 167);
            this.perprice.Name = "perprice";
            this.perprice.Size = new System.Drawing.Size(0, 24);
            this.perprice.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(26, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(124, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "請輸入數量：";
            // 
            // product_amount
            // 
            this.product_amount.Location = new System.Drawing.Point(162, 227);
            this.product_amount.Name = "product_amount";
            this.product_amount.Size = new System.Drawing.Size(116, 22);
            this.product_amount.TabIndex = 9;
            this.product_amount.TextChanged += new System.EventHandler(this.little_total);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(354, 227);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "小計：";
            // 
            // little_bill
            // 
            this.little_bill.AutoSize = true;
            this.little_bill.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.little_bill.Location = new System.Drawing.Point(427, 227);
            this.little_bill.Name = "little_bill";
            this.little_bill.Size = new System.Drawing.Size(0, 24);
            this.little_bill.TabIndex = 11;
            this.little_bill.TextChanged += new System.EventHandler(this.little_total);
            // 
            // addintobasket
            // 
            this.addintobasket.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.addintobasket.Location = new System.Drawing.Point(30, 290);
            this.addintobasket.Name = "addintobasket";
            this.addintobasket.Size = new System.Drawing.Size(95, 49);
            this.addintobasket.TabIndex = 12;
            this.addintobasket.Text = "加入購物籃";
            this.addintobasket.UseVisualStyleBackColor = true;
            this.addintobasket.Click += new System.EventHandler(this.addintobasket_Click);
            // 
            // total
            // 
            this.total.AutoSize = true;
            this.total.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.total.Location = new System.Drawing.Point(245, 303);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(0, 24);
            this.total.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(158, 300);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 27);
            this.label5.TabIndex = 14;
            this.label5.Text = "共計：";
            // 
            // order_log
            // 
            this.order_log.FormattingEnabled = true;
            this.order_log.ItemHeight = 12;
            this.order_log.Location = new System.Drawing.Point(118, 376);
            this.order_log.Name = "order_log";
            this.order_log.Size = new System.Drawing.Size(244, 196);
            this.order_log.TabIndex = 15;
            this.order_log.MouseDown += new System.Windows.Forms.MouseEventHandler(this.order_log_MouseDown);
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("微軟正黑體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label87.Location = new System.Drawing.Point(26, 376);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(86, 24);
            this.label87.TabIndex = 16;
            this.label87.Text = "已購買：";
            // 
            // order
            // 
            this.order.Font = new System.Drawing.Font("微軟正黑體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.order.Location = new System.Drawing.Point(482, 495);
            this.order.Name = "order";
            this.order.Size = new System.Drawing.Size(94, 77);
            this.order.TabIndex = 17;
            this.order.Text = "結帳";
            this.order.UseVisualStyleBackColor = true;
            this.order.Click += new System.EventHandler(this.order_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // Order_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 614);
            this.Controls.Add(this.order);
            this.Controls.Add(this.label87);
            this.Controls.Add(this.order_log);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.total);
            this.Controls.Add(this.addintobasket);
            this.Controls.Add(this.little_bill);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.product_amount);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.perprice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.choose_product);
            this.Controls.Add(this.combobox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.choose_store);
            this.Controls.Add(this.ClientName);
            this.Controls.Add(this.client);
            this.Name = "Order_Info";
            this.Load += new System.EventHandler(this.Order_Info_Load);
            ((System.ComponentModel.ISupportInitialize)(this.orderLogBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox client;
        private System.Windows.Forms.Label ClientName;
        private System.Windows.Forms.ComboBox choose_store;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label combobox;
        private System.Windows.Forms.ComboBox choose_product;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label perprice;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox product_amount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label little_bill;
        private System.Windows.Forms.Button addintobasket;
        private System.Windows.Forms.Label total;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox order_log;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Button order;
        private System.Windows.Forms.BindingSource orderLogBindingSource;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
    }
}