﻿using SQL_signin.Model;
using SQL_signin.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_signin
{
    public partial class Order_Info : MetroFramework.Forms.MetroForm
    {
        static Order_Info info = null;
        static int id;
        bool count = false;
        int product_Id;
        int ltt = 0, totalprice = 0;      
        List<OrderDetail> list = new List<OrderDetail>();

        public Order_Info()
        {
            InitializeComponent();           
        }

     

        public static Order_Info createinfo(int id) // 獨體模式
        {
            if (info == null || info.IsDisposed)
            {
                info = new Order_Info();
                Order_Info.id = id;                   
            }
            return info;
        }

        private void Order_Info_Load(object sender, EventArgs e)
        {
            using (PersonalInfoService personalInfoService = new PersonalInfoService())
            {
               SqlDataReader reader =  personalInfoService.getPersonalInfo(id);
               reader.Read();
               client.Text = reader["client_name"].ToString();
               reader.Close();
            }


            using (StoreService storeService = new StoreService())
            {
                choose_store.DataSource = storeService.getStoreName();

                choose_store.DisplayMember = "Key";
                choose_store.ValueMember = "Value";
            }
        }

        private void removefirst(object sender, EventArgs e)
        {
            if (!count)
            {
                List<ComboxModel> l = (List<ComboxModel>)choose_store.DataSource;
                choose_store.DataSource = null;
                l.RemoveAt(0);
                choose_store.DataSource = l;
                choose_store.DisplayMember = "Key";
                choose_store.ValueMember = "Value";
                count = true;
            }
        }

        private void choose_store_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (choose_store.SelectedValue is int)
            {
                int store_Id = Int32.Parse(choose_store.SelectedValue.ToString());
                using (StoreService service = new StoreService())
                {
                    choose_product.DataSource = service.getProductName(store_Id);
                    choose_product.DisplayMember = "Key";
                    choose_product.ValueMember = "Value";

                    product_amount.Text = "";
                }
            }
        }

        private void choose_product_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {          
            if (choose_product.SelectedValue is int) //第一次傳進來 因為沒有Value 所以為一個model 故要判斷是否為int
            {
                using (StoreService service = new StoreService())
                {
                    product_Id = (int)choose_product.SelectedValue;
                    perprice.Text = service.getProductPrice(product_Id);
                    product_amount.Text = "";
                }
            }
        }

        private void little_total(object sender, EventArgs e)
        {
            if (!product_amount.Text.Equals(""))
            {
                ltt = Int32.Parse(product_amount.Text) * Int32.Parse(perprice.Text);
                little_bill.Text = ltt.ToString();
            }else
            {
                little_bill.Text = "";
            }         
        }

        private void addintobasket_Click(object sender, EventArgs e)
        {
           
                totalprice += ltt;
                total.Text = totalprice.ToString();
                string product = choose_product.Text;
                int amount = Int32.Parse(product_amount.Text);
                string disaplay = product + " 數量: " + amount + " 共 " + ltt + " 元 ";

                order_log.Items.Add(disaplay);
      
                OrderDetail orderDetail = new OrderDetail();
            
                orderDetail.product_Id = product_Id;
                orderDetail.number = amount;
                orderDetail.perprice = (ltt / amount);

                list.Add(orderDetail);
                              
        }

        private void order_log_MouseDown(object sender, MouseEventArgs e)
        {          
            if(e.Button == MouseButtons.Right)
            {
                ListBox listBox = (ListBox)sender;
                if(listBox.SelectedItem != null)
                {
                    DialogResult dialogResult = MessageBox.Show("確認刪除項目？","" ,MessageBoxButtons.OKCancel);
                    if(dialogResult == DialogResult.OK)
                    {                                                                
                        string item = listBox.SelectedItem.ToString();                       
                        int index = item.IndexOf("共 ");
                        int lt = Int32.Parse(item.Substring(index + 2, item.Length - index - 2 - 3));
                        total.Text = (totalprice - lt).ToString();
                        totalprice -= ltt;

                        Console.WriteLine(lt);
                        
                        listBox.Items.RemoveAt(listBox.SelectedIndex);
                                                   
                    }
                }
                
            }
        }
      
        private void order_Click(object sender, EventArgs e)
        {
            using (OrderInfoService orderInfoService = new OrderInfoService())
            {
                int change = orderInfoService.createbill(id , DateTime.Now.ToString("yyyy-MM-dd"));
                int order_Id = orderInfoService.getCurrentOrderId(); 
                int create = orderInfoService.createdetail(order_Id, list);

                if (change >= 1 && create >= 1)
                {
                    MessageBox.Show("訂購成功！");
                    
                }else
                {
                    MessageBox.Show("訂購失敗！");
                }              
            }
        }       
    }
}
