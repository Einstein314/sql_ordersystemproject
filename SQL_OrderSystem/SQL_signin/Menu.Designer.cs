﻿namespace SQL_signin
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.order = new System.Windows.Forms.Button();
            this.myinfo = new System.Windows.Forms.Button();
            this.strinfo = new System.Windows.Forms.Button();
            this.history = new System.Windows.Forms.Button();
            this.metroLink1 = new MetroFramework.Controls.MetroLink();
            this.SuspendLayout();
            // 
            // order
            // 
            this.order.BackColor = System.Drawing.Color.Gold;
            this.order.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.order.Location = new System.Drawing.Point(302, 120);
            this.order.Name = "order";
            this.order.Size = new System.Drawing.Size(150, 135);
            this.order.TabIndex = 0;
            this.order.Text = "訂餐";
            this.order.UseVisualStyleBackColor = false;
            this.order.Click += new System.EventHandler(this.order_Click);
            // 
            // myinfo
            // 
            this.myinfo.BackColor = System.Drawing.Color.DarkOrange;
            this.myinfo.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.myinfo.Location = new System.Drawing.Point(95, 120);
            this.myinfo.Name = "myinfo";
            this.myinfo.Size = new System.Drawing.Size(150, 135);
            this.myinfo.TabIndex = 1;
            this.myinfo.Text = "個人資料";
            this.myinfo.UseVisualStyleBackColor = false;
            this.myinfo.Click += new System.EventHandler(this.myinfo_Click);
            // 
            // strinfo
            // 
            this.strinfo.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.strinfo.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.strinfo.Location = new System.Drawing.Point(95, 294);
            this.strinfo.Name = "strinfo";
            this.strinfo.Size = new System.Drawing.Size(150, 135);
            this.strinfo.TabIndex = 2;
            this.strinfo.Text = "店家資料";
            this.strinfo.UseVisualStyleBackColor = false;
            this.strinfo.Click += new System.EventHandler(this.strinfo_Click);
            // 
            // history
            // 
            this.history.BackColor = System.Drawing.Color.Green;
            this.history.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.history.Location = new System.Drawing.Point(302, 294);
            this.history.Name = "history";
            this.history.Size = new System.Drawing.Size(150, 135);
            this.history.TabIndex = 3;
            this.history.Text = "歷史資料查詢";
            this.history.UseVisualStyleBackColor = false;
            this.history.Click += new System.EventHandler(this.history_Click);
            // 
            // metroLink1
            // 
            this.metroLink1.Location = new System.Drawing.Point(-15, -15);
            this.metroLink1.Name = "metroLink1";
            this.metroLink1.Size = new System.Drawing.Size(75, 23);
            this.metroLink1.TabIndex = 4;
            this.metroLink1.Text = "metroLink1";
            this.metroLink1.UseSelectable = true;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 514);
            this.Controls.Add(this.metroLink1);
            this.Controls.Add(this.history);
            this.Controls.Add(this.strinfo);
            this.Controls.Add(this.myinfo);
            this.Controls.Add(this.order);
            this.Name = "Menu";
            this.Text = "Menu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button order;
        private System.Windows.Forms.Button myinfo;
        private System.Windows.Forms.Button strinfo;
        private System.Windows.Forms.Button history;
        private MetroFramework.Controls.MetroLink metroLink1;
    }
}