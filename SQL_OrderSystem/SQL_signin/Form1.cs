﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using SQL_signin.Service;

namespace SQL_signin
{
    public partial class Form1 : MetroFramework.Forms.MetroForm
    {
        String a, p;
        public Form1() 
        {
            InitializeComponent();
            
        }
        private void signin_Click(object sender, EventArgs e)
        {
            a = act.Text;
            p = psw.Text;

            using (LoginService service = new LoginService())
            {
                SqlDataReader reader = service.Login(a, p);

                if (reader.Read())
                {
                    MessageBox.Show("登入成功");
                    Menu menu = new Menu((int)reader["client_Id"]);
                    this.Hide();
                    menu.Show();
                }
                else
                {
                    MessageBox.Show("登入失敗");
                }
            }
        }
    }
}
