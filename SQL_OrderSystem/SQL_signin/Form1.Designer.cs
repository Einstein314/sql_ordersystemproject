﻿namespace SQL_signin
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.account = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.act = new System.Windows.Forms.TextBox();
            this.psw = new System.Windows.Forms.TextBox();
            this.signin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // account
            // 
            this.account.AutoSize = true;
            this.account.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.account.Location = new System.Drawing.Point(70, 106);
            this.account.Name = "account";
            this.account.Size = new System.Drawing.Size(57, 20);
            this.account.TabIndex = 0;
            this.account.Text = "帳號：";
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.password.Location = new System.Drawing.Point(70, 164);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(57, 20);
            this.password.TabIndex = 1;
            this.password.Text = "密碼：";
            // 
            // act
            // 
            this.act.Location = new System.Drawing.Point(131, 104);
            this.act.Name = "act";
            this.act.Size = new System.Drawing.Size(100, 22);
            this.act.TabIndex = 2;
            // 
            // psw
            // 
            this.psw.Location = new System.Drawing.Point(131, 162);
            this.psw.Name = "psw";
            this.psw.Size = new System.Drawing.Size(100, 22);
            this.psw.TabIndex = 3;
            // 
            // signin
            // 
            this.signin.Location = new System.Drawing.Point(156, 244);
            this.signin.Name = "signin";
            this.signin.Size = new System.Drawing.Size(75, 23);
            this.signin.TabIndex = 4;
            this.signin.Text = "登入";
            this.signin.UseVisualStyleBackColor = true;
            this.signin.Click += new System.EventHandler(this.signin_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 357);
            this.Controls.Add(this.signin);
            this.Controls.Add(this.psw);
            this.Controls.Add(this.act);
            this.Controls.Add(this.password);
            this.Controls.Add(this.account);
            this.Name = "Form1";
            this.Text = "OrderSystem";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label account;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.TextBox act;
        private System.Windows.Forms.TextBox psw;
        private System.Windows.Forms.Button signin;
    }
}

