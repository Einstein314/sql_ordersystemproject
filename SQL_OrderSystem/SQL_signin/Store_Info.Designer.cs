﻿namespace SQL_signin
{
    partial class Store_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.save = new System.Windows.Forms.Button();
            this.store_addresstxb = new System.Windows.Forms.TextBox();
            this.product = new System.Windows.Forms.Label();
            this.store_address = new System.Windows.Forms.Label();
            this.store_name = new System.Windows.Forms.Label();
            this.store_name_comboBox = new System.Windows.Forms.ComboBox();
            this.store_product_comboBox = new System.Windows.Forms.ComboBox();
            this.Price = new System.Windows.Forms.Label();
            this.productprice = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(617, 386);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(101, 34);
            this.save.TabIndex = 16;
            this.save.Text = "儲存";
            this.save.UseVisualStyleBackColor = true;
            // 
            // store_addresstxb
            // 
            this.store_addresstxb.Location = new System.Drawing.Point(178, 197);
            this.store_addresstxb.Name = "store_addresstxb";
            this.store_addresstxb.Size = new System.Drawing.Size(207, 22);
            this.store_addresstxb.TabIndex = 15;
            // 
            // product
            // 
            this.product.AutoSize = true;
            this.product.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.product.Location = new System.Drawing.Point(48, 276);
            this.product.Name = "product";
            this.product.Size = new System.Drawing.Size(106, 27);
            this.product.TabIndex = 12;
            this.product.Text = "販賣商品 :";
            // 
            // store_address
            // 
            this.store_address.AutoSize = true;
            this.store_address.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.store_address.Location = new System.Drawing.Point(48, 192);
            this.store_address.Name = "store_address";
            this.store_address.Size = new System.Drawing.Size(106, 27);
            this.store_address.TabIndex = 11;
            this.store_address.Text = "店家地址 :";
            // 
            // store_name
            // 
            this.store_name.AutoSize = true;
            this.store_name.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.store_name.Location = new System.Drawing.Point(48, 112);
            this.store_name.Name = "store_name";
            this.store_name.Size = new System.Drawing.Size(59, 27);
            this.store_name.TabIndex = 10;
            this.store_name.Text = "店名:";
            // 
            // store_name_comboBox
            // 
            this.store_name_comboBox.FormattingEnabled = true;
            this.store_name_comboBox.Location = new System.Drawing.Point(143, 112);
            this.store_name_comboBox.Name = "store_name_comboBox";
            this.store_name_comboBox.Size = new System.Drawing.Size(242, 20);
            this.store_name_comboBox.TabIndex = 19;
            this.store_name_comboBox.SelectedIndexChanged += new System.EventHandler(this.store_name_comboBox_SelectedIndexChanged);
            this.store_name_comboBox.Click += new System.EventHandler(this.removefirst);
            // 
            // store_product_comboBox
            // 
            this.store_product_comboBox.FormattingEnabled = true;
            this.store_product_comboBox.Location = new System.Drawing.Point(178, 283);
            this.store_product_comboBox.Name = "store_product_comboBox";
            this.store_product_comboBox.Size = new System.Drawing.Size(242, 20);
            this.store_product_comboBox.TabIndex = 20;
            this.store_product_comboBox.SelectedIndexChanged += new System.EventHandler(this.store_product_comboBox_SelectedIndexChanged);
            // 
            // Price
            // 
            this.Price.AutoSize = true;
            this.Price.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Price.Location = new System.Drawing.Point(48, 372);
            this.Price.Name = "Price";
            this.Price.Size = new System.Drawing.Size(106, 27);
            this.Price.TabIndex = 21;
            this.Price.Text = "商品價格 :";
            // 
            // productprice
            // 
            this.productprice.Location = new System.Drawing.Point(178, 377);
            this.productprice.Name = "productprice";
            this.productprice.Size = new System.Drawing.Size(207, 22);
            this.productprice.TabIndex = 22;
            // 
            // Store_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 491);
            this.Controls.Add(this.productprice);
            this.Controls.Add(this.Price);
            this.Controls.Add(this.store_product_comboBox);
            this.Controls.Add(this.store_name_comboBox);
            this.Controls.Add(this.save);
            this.Controls.Add(this.store_addresstxb);
            this.Controls.Add(this.product);
            this.Controls.Add(this.store_address);
            this.Controls.Add(this.store_name);
            this.Name = "Store_Info";
            this.Text = "StoreInfo";
            this.Load += new System.EventHandler(this.Store_Info_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.TextBox store_addresstxb;
        private System.Windows.Forms.Label product;
        private System.Windows.Forms.Label store_address;
        private System.Windows.Forms.Label store_name;
        private System.Windows.Forms.ComboBox store_name_comboBox;
        private System.Windows.Forms.ComboBox store_product_comboBox;
        private System.Windows.Forms.Label Price;
        private System.Windows.Forms.TextBox productprice;
    }
}