﻿using SQL_signin.Model;
using SQL_signin.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_signin
{
    public partial class Store_Info : MetroFramework.Forms.MetroForm
    {
        private bool count = false;
        static Store_Info info = null;
        public Store_Info()
        {
            InitializeComponent();
        }

     
        public static Store_Info createinfo() // 獨體模式
        {
            if (info == null || info.IsDisposed)
            {
                info = new Store_Info();
            }
            return info;
        }

        private void Store_Info_Load(object sender, EventArgs e)
        {
            using (StoreService storeService = new StoreService())
            {
                store_name_comboBox.DataSource = storeService.getStoreName();

                store_name_comboBox.DisplayMember = "Key";
                store_name_comboBox.ValueMember = "Value";
            }
        }

        private void removefirst(object sender, EventArgs e)
        {
            if (!count)
            {
                List<ComboxModel> l = (List<ComboxModel>)store_name_comboBox.DataSource;
                store_name_comboBox.DataSource = null;
                l.RemoveAt(0);
                store_name_comboBox.DataSource = l;
                store_name_comboBox.DisplayMember = "Key";
                store_name_comboBox.ValueMember = "Value";
                count = true;
            }
        }

        private void store_name_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (store_name_comboBox.SelectedValue is int)
            {
                int store_Id = Int32.Parse(store_name_comboBox.SelectedValue.ToString());
                using (StoreService service = new StoreService())
                {

                    store_addresstxb.Text = service.getStoreAddress(store_Id);

                    store_product_comboBox.DataSource = service.getProductName(store_Id);
                    store_product_comboBox.DisplayMember = "Key";
                    store_product_comboBox.ValueMember = "Value";
                }

                
            }
        }

        private void store_product_comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Console.WriteLine(store_product_comboBox.SelectedValue); 
            if (store_product_comboBox.SelectedValue is int) //第一次傳進來 因為沒有Value 所以為一個model 故要判斷是否為int
            {
                using (StoreService service = new StoreService())
                {
                    int product_Id = (int)store_product_comboBox.SelectedValue;
                    productprice.Text = service.getProductPrice(product_Id);
                }

            }
        }

      
    }
}
