﻿namespace SQL_signin
{
    partial class Personal_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.personal_name = new System.Windows.Forms.Label();
            this.personal_address = new System.Windows.Forms.Label();
            this.personal_account = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.account = new System.Windows.Forms.TextBox();
            this.address = new System.Windows.Forms.TextBox();
            this.save = new System.Windows.Forms.Button();
            this.personal_password = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // personal_name
            // 
            this.personal_name.AutoSize = true;
            this.personal_name.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.personal_name.Location = new System.Drawing.Point(48, 94);
            this.personal_name.Name = "personal_name";
            this.personal_name.Size = new System.Drawing.Size(101, 27);
            this.personal_name.TabIndex = 1;
            this.personal_name.Text = "您的名字:";
            // 
            // personal_address
            // 
            this.personal_address.AutoSize = true;
            this.personal_address.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.personal_address.Location = new System.Drawing.Point(48, 195);
            this.personal_address.Name = "personal_address";
            this.personal_address.Size = new System.Drawing.Size(106, 27);
            this.personal_address.TabIndex = 2;
            this.personal_address.Text = "您的地址 :";
            // 
            // personal_account
            // 
            this.personal_account.AutoSize = true;
            this.personal_account.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.personal_account.Location = new System.Drawing.Point(43, 306);
            this.personal_account.Name = "personal_account";
            this.personal_account.Size = new System.Drawing.Size(106, 27);
            this.personal_account.TabIndex = 3;
            this.personal_account.Text = "您的帳號 :";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(186, 99);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(207, 22);
            this.name.TabIndex = 4;
            // 
            // account
            // 
            this.account.Location = new System.Drawing.Point(186, 311);
            this.account.Name = "account";
            this.account.Size = new System.Drawing.Size(207, 22);
            this.account.TabIndex = 5;
            // 
            // address
            // 
            this.address.Location = new System.Drawing.Point(186, 203);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(207, 22);
            this.address.TabIndex = 6;
            // 
            // save
            // 
            this.save.Location = new System.Drawing.Point(500, 407);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(101, 34);
            this.save.TabIndex = 7;
            this.save.Text = "儲存";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // personal_password
            // 
            this.personal_password.AutoSize = true;
            this.personal_password.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.personal_password.Location = new System.Drawing.Point(43, 410);
            this.personal_password.Name = "personal_password";
            this.personal_password.Size = new System.Drawing.Size(106, 27);
            this.personal_password.TabIndex = 8;
            this.personal_password.Text = "您的密碼 :";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(186, 415);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(207, 22);
            this.password.TabIndex = 9;
            // 
            // Personal_Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(761, 526);
            this.Controls.Add(this.password);
            this.Controls.Add(this.personal_password);
            this.Controls.Add(this.save);
            this.Controls.Add(this.address);
            this.Controls.Add(this.account);
            this.Controls.Add(this.name);
            this.Controls.Add(this.personal_account);
            this.Controls.Add(this.personal_address);
            this.Controls.Add(this.personal_name);
            this.Name = "Personal_Info";
            this.Text = " PersonalInfo";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label personal_name;
        private System.Windows.Forms.Label personal_address;
        private System.Windows.Forms.Label personal_account;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox account;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Label personal_password;
        private System.Windows.Forms.TextBox password;
    }
}