﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_signin.Service
{
    class LoginService : ConnectSQL
    {
        public SqlDataReader Login(String account, String password)
        {
            String sql = $"select client_Id from Client where account=@account and password=@password ";
            SqlCommand command = this.createSqlCommand(sql);
            command.Parameters.AddWithValue("@account", account);
            command.Parameters.AddWithValue("@password", password);

            SqlDataReader reader = command.ExecuteReader();

            return reader;
        }
    }
}
