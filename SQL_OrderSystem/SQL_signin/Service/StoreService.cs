﻿using SQL_signin.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_signin.Service
{
    class StoreService : ConnectSQL
    {
        public List<ComboxModel> getStoreName()
        {
            String sql = $"select * from Store ";
            SqlCommand command = this.createSqlCommand(sql);
            SqlDataReader reader = command.ExecuteReader();
            List<ComboxModel> list = new List<ComboxModel>();

            list.Add(new ComboxModel()
            {
                Key = "請選取資料",  //被後選有問題
                Value = 0
            });

            while (reader.Read())
            {
                ComboxModel model = new ComboxModel();
                model.Key = reader["store_name"].ToString();
                model.Value = Int32.Parse(reader["store_Id"].ToString());

                list.Add(model);
            }
            return list;
        }

        public String getStoreAddress(int store_Id)
        {
            String sql_address = $"select store_address from Store where store_Id = {store_Id}";
            SqlCommand command = this.createSqlCommand(sql_address);
            SqlDataReader reader_address = command.ExecuteReader();
            reader_address.Read();

            String address = reader_address["store_address"].ToString();

            reader_address.Close();

            return address;
        }
         
        public List<ComboxModel> getProductName(int store_Id)
        {
            String sql_product = $"select product_name, product_Id from Product where store_Id = {store_Id}";
            SqlCommand command_product = this.createSqlCommand(sql_product);
            SqlDataReader reader_product = command_product.ExecuteReader();
            List<ComboxModel> list = new List<ComboxModel>();

            while (reader_product.Read())
            {
                ComboxModel model = new ComboxModel();
                model.Key = reader_product["product_name"].ToString();
                model.Value = Int32.Parse(reader_product["product_Id"].ToString());

                list.Add(model);
            }
            

            return list;
        }

        public String getProductPrice(int product_Id)
        {
            String sql_price = $"select price from Product where product_Id = {product_Id}";
            SqlCommand command_price = this.createSqlCommand(sql_price);
            SqlDataReader reader_price = command_price.ExecuteReader();
            reader_price.Read();

            String price = reader_price["price"].ToString();

            return price;
        }
    }
}
