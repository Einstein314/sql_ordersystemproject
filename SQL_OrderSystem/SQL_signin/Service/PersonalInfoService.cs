﻿using SQL_signin.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_signin.Service
{
    class PersonalInfoService : ConnectSQL
    {       

        public SqlDataReader getPersonalInfo(int myid)
        {
            String sql = $"select * from Client where client_Id = {myid} ";

            SqlCommand command = this.createSqlCommand(sql);
            SqlDataReader reader = command.ExecuteReader();
                    
            return reader;
        }

        public int setPersonalInfo(int myid, String n, String ad, String ac, String p)
        {
            String sql = $@"update Client 
                            set client_name = '{n}', client_address = '{ad}', account = '{ac}', password = '{p}'
                            where client_Id = {myid}";
            SqlCommand sqlCommand = this.createSqlCommand(sql);
            int reader = sqlCommand.ExecuteNonQuery();

            return reader;
        }

        public List<ClientName> getClientName()
        {
            List<ClientName> list = new List<ClientName>();
            
            String sql = $@"select client_name, client_Id from Client";

            SqlCommand command = this.createSqlCommand(sql);
            SqlDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                 ClientName name = new ClientName();
                 name.clientName = reader["client_name"].ToString();
                 name.value = Int32.Parse(reader["client_Id"].ToString());                

                 list.Add(name);
            }
           
            return list;
        }
    }
}
