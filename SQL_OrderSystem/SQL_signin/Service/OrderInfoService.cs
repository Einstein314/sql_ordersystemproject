﻿using SQL_signin.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQL_signin.Service
{
    class OrderInfoService : ConnectSQL
    {
        public int createbill(int client_Id, string order_date)
        {            
            string sql = $"insert into OrderTable(client_Id, order_date) values({client_Id}, '{order_date}')";
            SqlCommand command = this.createSqlCommand(sql);
            int reader = command.ExecuteNonQuery();

            return reader;
        }

        public int getCurrentOrderId()
        {
            string findsql = $"select IDENT_CURRENT('OrderTable')  as order_Id";
            SqlCommand findcommand = this.createSqlCommand(findsql);
            SqlDataReader dataReader = findcommand.ExecuteReader();
            dataReader.Read();
            int order_Id = Int32.Parse(dataReader["order_Id"].ToString());

            dataReader.Close();

            return order_Id;
        }

        public int createdetail(int order_Id, List<OrderDetail> orderDetails)
        {
            int reader = 0;
            for(int i=0; i<orderDetails.Count; i++)
            {
                string sql = $@"insert into Order_Detail(order_Id, product_Id, total_price, number) values({order_Id}, {orderDetails[i].product_Id}, {orderDetails[i].perprice}, {orderDetails[i].number})";
                SqlCommand command = this.createSqlCommand(sql);
                reader += command.ExecuteNonQuery();
            }
        
            return reader;
        }
 
        public List<Order> history(int clientValue, DateTime first, DateTime last)
        {
            List<Order> list = new List<Order>();

            StringBuilder sqlStringBuilder = new StringBuilder($"select CAST(order_Id as varchar) + ' (' + CAST(order_date as varchar) + ') ' as orderData, order_Id from OrderTable where 1=1");         
            
            if (clientValue != 0)
            {
                sqlStringBuilder.Append($" and client_Id = {clientValue}");
             
            }
            if(!first.ToString("yyyy-MM-dd").Equals(DateTime.Now.ToString("yyyy-MM-dd")) && !last.ToString("yyyy-MM-dd").Equals(DateTime.Now.ToString("yyyy-MM-dd")))
            {
                string f = first.ToString("yyyy-MM-dd");
                string l = last.ToString("yyyy-MM-dd");
                sqlStringBuilder.Append($" and order_date between '{f}' and '{l}' ");               
            }

            string sql = sqlStringBuilder.ToString();
            Console.WriteLine(sql);
            SqlCommand command = this.createSqlCommand(sql);
            SqlDataReader reader = command.ExecuteReader();
            while(reader.Read())
            {
                Order order = new Order();
                order.Key = reader["orderData"].ToString();
                order.Value = Int32.Parse(reader["order_Id"].ToString());

                list.Add(order);
            }

            return list;

        }

        public List<OrderHistory> getHistory(int order_Id)
        {          
            List<OrderHistory> list = new List<OrderHistory>();

            string sql = $@"select order_Id, store_name, product_name, total_price, number, total_price*number as littletotal  from Order_Detail 
                            join Product on Order_Detail.product_Id = Product.product_Id
                            join Store on Product.store_Id = Store.store_Id
                            where order_Id = {order_Id}";
            SqlCommand command = this.createSqlCommand(sql);
            SqlDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                OrderHistory history = new OrderHistory();

                history.order_Id = Int32.Parse(reader["order_Id"].ToString());
                history.storeName = reader["store_name"].ToString();
                history.productName = reader["product_name"].ToString();
                history.perprice = Int32.Parse(reader["total_price"].ToString());
                history.amount = Int32.Parse(reader["number"].ToString());
                history.littletotal = Int32.Parse(reader["littletotal"].ToString());

                list.Add(history);

            }

            return list;
        }
    }
}
