﻿using SQL_signin.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_signin
{
    public partial class Menu : MetroFramework.Forms.MetroForm
    {
        public int id;
        public Menu(int id)
        {
            InitializeComponent();           
            this.id = id;
        }

        private void myinfo_Click(object sender, EventArgs e)
        {
            Personal_Info.createinfo(id).Show();  
          
        }

        private void order_Click(object sender, EventArgs e)
        {
            Order_Info.createinfo(id).Show();
        }

        private void strinfo_Click(object sender, EventArgs e)
        {
            Store_Info.createinfo().Show();
        }

        private void history_Click(object sender, EventArgs e)
        {
            Detail.createinfo().Show();
        }      
    }
}
