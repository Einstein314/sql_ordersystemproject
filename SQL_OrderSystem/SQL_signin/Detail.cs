﻿using SQL_signin.Model;
using SQL_signin.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_signin
{
    public partial class Detail : MetroFramework.Forms.MetroForm
    {
        static Detail info = null;

        public Detail()
        {
            InitializeComponent();
        }


        public static Detail createinfo() // 獨體模式
        {
            if (info == null || info.IsDisposed)
            {
                info = new Detail();
            }
            return info;
        }

        private void Detail_Load(object sender, EventArgs e)
        {
            using (PersonalInfoService service = new PersonalInfoService())
            {
                List<ClientName> list = service.getClientName();
                ClientName name = new ClientName();
                name.clientName = "請選擇訂購人";
                name.value = 0;

                list.Insert(0, name);

                selectClient.DataSource = list;
                selectClient.DisplayMember = "clientName";
                selectClient.ValueMember = "value";
            }
        }

        private void clientChange(object sender, EventArgs e)
        {
            selectOrder.DataSource = null;
            selectOrder.Text = "";
            detaildata.DataSource = null;           
            selectFirstDate.Text = DateTime.Now.ToShortDateString();
            selectLastDate.Text = DateTime.Now.ToShortDateString();
        }

        private void search_Click(object sender, EventArgs e)
        {
            int clientValue = (int)selectClient.SelectedValue;
            DateTime first = selectFirstDate.Value;
            DateTime last = selectLastDate.Value;
            using (OrderInfoService service = new OrderInfoService())
            {
                selectOrder.DataSource = service.history(clientValue, first, last);
                selectOrder.ValueMember = "Value";
                selectOrder.DisplayMember = "Key";
            }
        }

        private void selectOrderData(object sender, EventArgs e)
        {
            if(selectOrder.SelectedValue is int)
            {
                using (OrderInfoService service = new OrderInfoService())
                {
                    
                    detaildata.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                    List<OrderHistory> list = service.getHistory((int)selectOrder.SelectedValue);
                    detaildata.DataSource = list;                
                }
            }           
        }

        private void changeTitle(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            detaildata.Columns[0].HeaderText = "訂單編號";
            detaildata.Columns[1].HeaderText = "店家名稱";
            detaildata.Columns[2].HeaderText = "產品名稱";
            detaildata.Columns[3].HeaderText = "單價";
            detaildata.Columns[4].HeaderText = "數量";
            detaildata.Columns[5].HeaderText = "小計";
        }

      
    }

    public class OrderHistory
    {
        public int order_Id { get; set; }
        public string storeName { get; set; }
        public string productName { get; set; }
        public int perprice { get; set; }
        public int amount { get; set; }
        public int littletotal { get; set; }
    }
}
