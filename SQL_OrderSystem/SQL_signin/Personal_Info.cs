﻿using SQL_signin.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SQL_signin
{
    public partial class Personal_Info : MetroFramework.Forms.MetroForm
    {
        private static Personal_Info info;
        public static int myid;     
        private Personal_Info()
        {
            InitializeComponent();

            using (PersonalInfoService service = new PersonalInfoService())
            {
                SqlDataReader reader = service.getPersonalInfo(myid);
                while (reader.Read())
                {
                    name.Text = reader["client_name"].ToString();
                    address.Text = reader["client_address"].ToString();
                    account.Text = reader["account"].ToString();
                    password.Text = reader["password"].ToString();
                }
            }         
        }
    

        public static Personal_Info createinfo(int myid) // 獨體模式 //類別進入點
        {
            if (info == null || info.IsDisposed)
            {
                Personal_Info.myid = myid;         
                info = new Personal_Info();              
            }
            return info;
        }

        private void save_Click(object sender, EventArgs e)
        {                   
            using (PersonalInfoService service = new PersonalInfoService())
            {

                String n = name.Text.ToString(), ad = address.Text.ToString(), ac = account.Text.ToString(), p = password.Text.ToString();
                int reader = service.setPersonalInfo(myid, n, ad, ac, p);
                    
                if (reader > 0)
                {
                    MessageBox.Show("更新成功");
                }
                else
                {
                    MessageBox.Show("更新失敗");
                }
            }                          
        }     
    }
}
