﻿namespace SQL_signin
{
    partial class Detail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.detaildata = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.selectClient = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.selectFirstDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.selectLastDate = new System.Windows.Forms.DateTimePicker();
            this.search = new System.Windows.Forms.Button();
            this.selectOrder = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.detaildata)).BeginInit();
            this.SuspendLayout();
            // 
            // detaildata
            // 
            this.detaildata.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.detaildata.Location = new System.Drawing.Point(23, 213);
            this.detaildata.Name = "detaildata";
            this.detaildata.RowTemplate.Height = 24;
            this.detaildata.Size = new System.Drawing.Size(666, 299);
            this.detaildata.TabIndex = 0;
            this.detaildata.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.changeTitle);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(20, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "訂購人：";
            // 
            // selectClient
            // 
            this.selectClient.FormattingEnabled = true;
            this.selectClient.Location = new System.Drawing.Point(98, 74);
            this.selectClient.Name = "selectClient";
            this.selectClient.Size = new System.Drawing.Size(121, 20);
            this.selectClient.TabIndex = 2;
            this.selectClient.SelectedIndexChanged += new System.EventHandler(this.clientChange);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(259, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "訂購日期：";
            // 
            // selectFirstDate
            // 
            this.selectFirstDate.Location = new System.Drawing.Point(353, 70);
            this.selectFirstDate.Name = "selectFirstDate";
            this.selectFirstDate.Size = new System.Drawing.Size(135, 22);
            this.selectFirstDate.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(494, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(16, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "~";
            // 
            // selectLastDate
            // 
            this.selectLastDate.Location = new System.Drawing.Point(516, 70);
            this.selectLastDate.Name = "selectLastDate";
            this.selectLastDate.Size = new System.Drawing.Size(135, 22);
            this.selectLastDate.TabIndex = 7;
            // 
            // search
            // 
            this.search.Location = new System.Drawing.Point(686, 53);
            this.search.Name = "search";
            this.search.Size = new System.Drawing.Size(82, 41);
            this.search.TabIndex = 8;
            this.search.Text = "查詢";
            this.search.UseVisualStyleBackColor = true;
            this.search.Click += new System.EventHandler(this.search_Click);
            // 
            // selectOrder
            // 
            this.selectOrder.FormattingEnabled = true;
            this.selectOrder.Location = new System.Drawing.Point(145, 136);
            this.selectOrder.Name = "selectOrder";
            this.selectOrder.Size = new System.Drawing.Size(176, 20);
            this.selectOrder.TabIndex = 9;
            this.selectOrder.SelectedIndexChanged += new System.EventHandler(this.selectOrderData);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(19, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 16);
            this.label4.TabIndex = 10;
            this.label4.Text = "選擇查詢訂單：";
            // 
            // Detail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 574);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.selectOrder);
            this.Controls.Add(this.search);
            this.Controls.Add(this.selectLastDate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selectFirstDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.selectClient);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.detaildata);
            this.Name = "Detail";
            this.Text = "Detail";
            this.Load += new System.EventHandler(this.Detail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.detaildata)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView detaildata;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox selectClient;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker selectFirstDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker selectLastDate;
        private System.Windows.Forms.Button search;
        private System.Windows.Forms.ComboBox selectOrder;
        private System.Windows.Forms.Label label4;
    }
}