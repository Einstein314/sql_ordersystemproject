﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace SQL_signin
{
    public  class ConnectSQL:IDisposable
    {

        private SqlConnection sqlConnection;
        String constr = ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString;
        public ConnectSQL()
        {
           sqlConnection = new SqlConnection(constr);
           sqlConnection.Open();          
        }
            
        public SqlCommand createSqlCommand(String sql)
        {           
            SqlCommand sqlCommand = new SqlCommand(sql, sqlConnection);
            return sqlCommand;
        }

        public void Dispose()
        {
            this.sqlConnection.Close();
        }
    }
}
